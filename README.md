## How to install the Mobile App
1. Download and install Windows App from the folder "Client Installer - Windows"
2. Download and install Android App from the folder "Client Installer - Android"
3. Download and install server components from the folder "Server Installer"

## Training Videos on YouTube
Go to [https://www.youtube.com/watch?v=A49B6OEj4lY&list=PLrY0roojbpPV0GWtFx6J5kihJMHvue2k7](https://www.youtube.com/watch?v=A49B6OEj4lY&list=PLrY0roojbpPV0GWtFx6J5kihJMHvue2k7)

## Source code
1. Webservices go to: [https://gitlab.com/CIP-Development/GRIN-Global-WCFService](https://gitlab.com/CIP-Development/GRIN-Global-WCFService)
2. Mobile app Go to [https://gitlab.com/CIP-Development/inventorymobileapp](https://gitlab.com/CIP-Development/inventorymobileapp)

## Support
For support contact to: Edwin Rojas (e.rojas@cgiar.org) or by Skype: rojas556